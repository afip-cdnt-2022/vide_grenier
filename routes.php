<?php

use Src\Facades\Route;


Route::get('/', [\App\Controllers\HomeController::class, 'index'], 'home.index');
Route::get('/infoCIL', [\App\Controllers\HomeController::class, 'info'], 'home.info');

Route::get('/connexion', [\App\Controllers\Auth\AuthController::class, 'signIn'], 'auth.signIn');
Route::post('/connexion', [\App\Controllers\Auth\AuthController::class, 'login'], 'auth.login');
Route::get('/logout', [\App\Controllers\Auth\AuthController::class, 'logout'], 'auth.logout');

Route::get('/videgreniers', [\App\Controllers\VideGrenierController::class, 'index'], 'videgrenier.index');
Route::get('videgreniers/{id}/reservation', [\App\Controllers\VideGrenierController::class, 'form_resa'],'videgrenier.form_resa');
Route::post('videgreniers/{id}/reservation', [\App\Controllers\VideGrenierController::class, 'new_resa'],'videgrenier.new_resa');

Route::get('/mailing_list', [\App\Controllers\MailingListController::class, 'index'], 'mailing.index');
Route::post('/mailing_list', [\App\Controllers\MailingListController::class, 'create'], 'mailing.create');
Route::post('/mailing_list/delete', [\App\Controllers\MailingListController::class, 'delete'], 'mailing.delete');

Route::get('/mon_compte', [\App\Controllers\UtilisateurController::class, 'show'], 'utilisateur.show');
Route::get('/inscription', [\App\Controllers\UtilisateurController::class, 'new'], 'utilisateur.new');
Route::post('/inscription', [\App\Controllers\UtilisateurController::class, 'create'], 'utilisateur.create');
Route::post('/utilisateur/update', [\App\Controllers\UtilisateurController::class, 'update'], 'utilisateur.update');



Route::get('admin/dashboard', [\App\Controllers\AdminController::class, 'index'],'admin.index');

Route::get('admin/videgreniers/liste', [\App\Controllers\AdminController::class, 'liste'],'admin.videgrenier.liste');
Route::get('admin/videgreniers/new', [\App\Controllers\AdminController::class, 'new'],'admin.videgrenier.new');
Route::post('admin/videgreniers', [\App\Controllers\VideGrenierController::class, 'create'],'admin.videgrenier.create');
Route::get('admin/videgreniers/{id}/edit', [\App\Controllers\AdminController::class, 'editVg'],'admin.videgrenier.edit');
Route::post('admin/videgreniers/{id}/update', [\App\Controllers\VideGrenierController::class, 'update'],'admin.videgrenier.update');

Route::get('admin/utilisateurs', [\App\Controllers\AdminController::class, 'utilisateurs'],'admin.utilisateur.liste');
Route::post('admin/utilisateurs/delete', [\App\Controllers\UtilisateurController::class, 'delete'],'admin.utilisateur.delete');

Route::get('admin/attente', [\App\Controllers\AdminController::class, 'attentes'],'admin.attente');
Route::get('admin/reservation/{id}', [\App\Controllers\ReservationController::class, 'show'],'admin.resa.show');
Route::post('admin/reservation/{id}/update', [\App\Controllers\ReservationController::class, 'update'],'admin.resa.update');

Route::get('admin/mailing', [\App\Controllers\AdminController::class, 'mailing'],'admin.mailing.liste');
Route::post('admin/mailing/delete', [\App\Controllers\MailingListController::class, 'admin_delete'],'admin.mailing.delete');
