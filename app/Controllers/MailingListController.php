<?php


namespace App\Controllers;


use App\Models\Mail;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class MailingListController
{
    public function index()
    {
        return View::render('mailing_list');
    }

    public function create(array $data)
    {
        $mail = Database::prepare('SELECT * FROM mailing_list WHERE mail_ml = :mail', ['mail' => $data['MAIL_ML']], null, true);
        if($mail !== false){
            return Route::back()->withError(['erreur_mailing' => '*Cette adresse e-mail est déjà utilisée']);
        }
        Mail::create($data);
        return View::render('verif_mailing');
    }

    public function delete(array $data)
    {
        $mail = Database::prepare('SELECT * FROM mailing_list WHERE mail_ml = :mail', ['mail' => $data['MAIL_ML']], Mail::class, true);
        if($mail === false){
            return Route::back()->withError(['erreur_cancel_mailing' => '*Cette adresse e-mail n\'est pas utilisée']);
        }
        Mail::delete($mail->ID_ML);
        return View::render('verif_cancel_mailing');
    }

    public function admin_delete(array $data)
    {
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }
        Mail::delete($data['choix']);
        return View::render('admin/erase_ml');
    }
}