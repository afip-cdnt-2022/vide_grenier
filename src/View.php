<?php


namespace Src;


use Exception;
use Src\TwigExtensions\ViewExtension;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extra\Intl\IntlExtension;
use Twig\Loader\FilesystemLoader;

class View
{
    /**
     * Displays the rendering of the view on the site
     *
     * @param string $view
     * @param array $params
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public static function render(string $view, array $params = [])
    {
        $view .= '.twig';
        $loader = new FilesystemLoader(ROOT.'/views');
        $twig = new Environment($loader);
        $extensions = include 'TwigExtensions/extensions.php';
        $twig->addExtension(new IntlExtension());
        foreach ($extensions as $extension){
            $twig->addExtension(new $extension());
        }
        
        $twig->addGlobal('app', ['session' =>($_SESSION ?? []), 'get' => ($_GET ?? []), 'post' => ( $_POST ?? []), 'cookie' =>($_COOKIE ?? [])]);
        $twig->addGlobal('errors', Session::getInstance()->getFlashMessagesByType(Session::FLASH_ERROR));
        $twig->addGlobal('success', Session::getInstance()->getFlashMessagesByType(Session::FLASH_SUCCESS));
        $twig->addGlobal('flashMessages', Session::getInstance()->getFlashMessagesByType(Session::FLASH_MESSAGE));

        echo $twig->render($view, $params);
    }
}