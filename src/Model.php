<?php


namespace Src;


use Exception;
use ReflectionClass;
use ReflectionException;

class Model
{
    protected string $table;
    protected string $primaryKey;

    /**
     * Get the model table name
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * Get the model primary key
     *
     * @return string|null
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * Get the model current instance
     *
     * @return Model|object
     * @throws ReflectionException
     */
    protected static function getCurrentModel()
    {
        $current = new ReflectionClass(get_called_class());
        return $current->newInstance();
    }

    /**
     * Get a record
     *
     * @param mixed $id
     * @return Model
     * @throws ReflectionException
     */
    public static function find($id)
    {
        return Database::query(sprintf("SELECT * FROM %s WHERE %s = %s LIMIT 1", self::getCurrentModel()->getTable(), self::getCurrentModel()->getPrimaryKey(), $id), get_class(self::getCurrentModel()), true);
    }

    /**
     * Get all records
     *
     * @return array
     */
    public static function all(): array
    {
        return Database::query(sprintf("SELECT * FROM %s", self::getCurrentModel()->getTable()), get_class(self::getCurrentModel()));
    }

    /**
     * Get the number of records
     *
     * @return mixed
     * @throws Exception
     */
    public static function count(): int
    {
        $count = Database::query(sprintf("SELECT %s, COUNT(%s) AS count FROM %s"),
            self::getCurrentModel()->getPrimaryKey(), self::getCurrentModel()->getPrimaryKey(), self::getCurrentModel()->getTable());
        return $count;
    }

    /**
     * Records a new data in table
     *
     * @param array $data
     * @param string $table
     * @return Model|bool
     * @throws ReflectionException
     */
    public static function create(array $data, string $table = '')
    {
        $table = $table ?: self::getCurrentModel()->getTable();
        $data = self::filterColumns($data);
        $keys = [];
        $values = [];
        $inter = [];
        foreach ($data as $k => $v) {
            $keys[] = $k;
            $values[] = $v;
            $inter[] = '?';
        }
        $statement =
            'INSERT INTO ' .
            $table .
            ' (' .
            implode(',', $keys) .
            ')';
        $statement .= ' VALUES (' . implode(',', $inter) . ')';
        Database::prepare($statement, $values);
        return self::last();
    }

    /**
     * Update a record
     *
     * @param $id
     * @param array $data
     * @return bool|$this
     * @throws ReflectionException
     */
    public static function update($id, array $data)
    {
        $keys = [];
        $values = [];
        $data = self::filterColumns($data);
        foreach ($data as $k => $v) {
            $keys[] = sprintf('%s = ?', $k);
            $values[] = $v;
        }
        $values[] = $id;
        $statement =
            'UPDATE ' .
            self::getCurrentModel()->getTable() .
            ' SET ' .
            implode(', ', $keys) .
            ' WHERE ' .
            self::getCurrentModel()->getPrimaryKey() .
            ' = ?';
        Database::prepare($statement, $values);
        return self::find($id);
    }

    /**
     * Delete a record
     *
     * @param $id
     * @return array
     * @throws ReflectionException
     */
    public static function delete($id): array
    {
        $statement =
            'DELETE FROM ' .
            self::getCurrentModel()->getTable() .
            ' WHERE ' .
            self::getCurrentModel()->getPrimaryKey() .
            ' = ?';
        Database::prepare($statement, [$id]);
        return self::all();
    }

    public static function last()
    {
        return Database::query(sprintf("SELECT * FROM %s ORDER BY %s DESC LIMIT 1", self::getCurrentModel()->getTable(), self::getCurrentModel()->getPrimaryKey()), get_class(self::getCurrentModel()), true);
    }

    /**
     * Get the name of table column
     *
     * @return array
     * @throws ReflectionException
     */
    private static function getColumns()
    {
        return array_map(function ($column) {
            return $column['Field'];
        }, Database::query("SHOW COLUMNS FROM " . self::getCurrentModel()->getTable()));
    }

    /**
     * Filter needed column
     *
     * @param array $data
     * @return array
     * @throws ReflectionException
     */
    private static function filterColumns(array $data)
    {
        $columns = self::getColumns();
        $filteredData = [];
        foreach ($columns as $key => $column) {
            $filteredData[$column] = $data[$column] ?? $data[strtolower($column)] ?? null;
        }
        return array_filter($filteredData, function ($data) {
            return $data;
        });
    }

    public function __get($key)
    {
        if(method_exists($this, $key)){
            return $this->$key();
        }
        return null;
    }
}