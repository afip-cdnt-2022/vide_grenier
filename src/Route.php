<?php


namespace Src;


use Psr\Http\Message\ServerRequestInterface;

class Route
{
    private string $path;

    private array $action;

    private string $name;

    private array $matches;

    /**
     * @param string $path
     * @param array $action
     * @param string $name
     */
    public function __construct(string $path, array $action, string $name = '')
    {
        $this->path = '/' . trim($path, '/');
        $this->action = $action;
        $this->name = $name;
    }

    /**
     * Control route and run route callback
     *
     * @param ServerRequestInterface $request
     * @return void|bool
     */
    public function execute(ServerRequestInterface $request)
    {
        $params = [];
        if($this->matches){
            $params = $this->matches;
        }
        if($request->getParsedBody()){
            $params[] = $request->getParsedBody();
        }
        if(is_array($this->action)){
            $controller = new $this->action[0]();
            $method = $this->action[1];
            return call_user_func_array([$controller, $method], $params);
        }
    }

    /**
     * Check if url match wih one route and get params
     *
     * @param ServerRequestInterface $request
     * @return bool
     */
    public function match(ServerRequestInterface $request)
    {
        $url = trim($request->getUri()->getPath(), '/');
        $path = preg_replace('#({[\w]+})#', '([^/]+)', trim($this->path, '/'));

        $pathToMatch = "#^$path$#";
        if(preg_match($pathToMatch, $url, $matches)){
            $key = array_map(function ($pa) {
                if(preg_match('/(.*)?{(.*)?}/', $pa, $matches)){
                    return $matches[2];
                }
                return null;
            }, explode('/', trim($this->path, '/')));
            $key = array_filter($key);
            array_shift($matches);
            $matches = $matches != [] ? array_combine($key, $matches) : $matches;
            $this->matches = $matches;
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function __get($key)
    {
        if(method_exists($this, 'get'.ucfirst($key))){
            return $this->{'get'.ucfirst($key)}();
        }
        return null;
    }
}