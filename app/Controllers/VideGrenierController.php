<?php


namespace App\Controllers;


use App\Models\Reservation;
use App\Models\VideGrenier;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class VideGrenierController
{
    public function index()
    {
        $videgreniers = VideGrenier::all();
        return View::render('vide_grenier', compact('videgreniers'));
    }

    public function form_resa($id)
    {
        $videgrenier = VideGrenier::find($id);
        return View::render('reservation', compact('videgrenier'));
    }

    public function new_resa($id, array $data)
    {
        $reservation = Database::prepare('SELECT * FROM reservation_vg WHERE mail_resa = :doublon AND id_vg = :id', [
            'doublon' => $data['MAIL_RESA'],
            'id' => $id
        ], null, true);
        if($reservation !== false){
            return Route::back()->withError(['erreur_reservation' => '*Cette adresse e-mail est déjà utilisée pour cette réservation']);
        }
        $videgrenier = VideGrenier::find($id);
        $nbrPlaceRestantApresResa = (int) $videgrenier->NBR_RESTANT_VG - (int) $data['NBR_RESA'];

        if($nbrPlaceRestantApresResa < 0){
            return Route::back()->withError(['erreur_reservation' => '*Désoler, plus de places disponibles']);
        }
        VideGrenier::update($id, ['NBR_RESTANT_VG' => $nbrPlaceRestantApresResa]);
        $data['ID_VG'] = $id;
        $data['ID_UTIL'] = AuthManager::getInstance()->getAuth(true);
        Reservation::create($data);
        return View::render('verif_resa');
    }

    public function create(array $data)
    {
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }
        if($data['HEURE_DEBUT_VG'] >= $data['HEURE_FIN_VG']){
            return Route::back()->withError(['erreur_reservation' => '*La date de début doit être ulterieur à la date de fin']);
        }
        $data['NBR_RESTANT_VG'] = $data['NBR_EMPLACEMENTS'];
        VideGrenier::create($data);
        return View::render('admin/verif_prog_vg');
    }

    public function update($id, array $data){
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }
        if($data['HEURE_DEBUT_VG'] >= $data['HEURE_FIN_VG']){
            return Route::back()->withError(['erreur_prog' => '*La date de début doit être ulterieur à la date de fin']);
        }
        VideGrenier::update($id, $data);
        return View::render('admin/verif_update_vg');
    }
}