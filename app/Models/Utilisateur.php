<?php


namespace App\Models;


use Src\Model;

class Utilisateur extends Model
{
    protected string $primaryKey = 'ID_UTIL';
    protected string $table = 'utilisateur';
    
    public static function create(array $data, string $table='')
    {
        $data['mdp_util'] = password_hash($data['mdp_util'], PASSWORD_DEFAULT);
        parent::create($data, $table);
    }
}