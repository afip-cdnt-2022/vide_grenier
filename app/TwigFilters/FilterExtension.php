<?php


namespace App\TwigFilters;


use NumberFormatter;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FilterExtension extends \Twig\Extension\AbstractExtension
{
    public function getFilters()
    {
        $twigFunctions = [];
        $names = ['truncate', 'uppercase', 'euro'];
        foreach ($names as $name){
            $twigFunctions[] = new TwigFilter($name, [$this, $name]);
        }
        return $twigFunctions;
    }

    public function truncate(string $text, int $length = 30): string
    {
        if(strlen($text) > $length){
            return substr($text, 0, $length).'...';
        }
        return $text;
    }

    public function uppercase(string $text): string
    {
        return mb_strtoupper($text);
    }

    public function euro($prix)
    {
        $formatter = new NumberFormatter('fr_FR',  NumberFormatter::CURRENCY);
        echo $formatter->formatCurrency($prix, 'EUR');
    }
}