-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 20 juil. 2020 à 08:37
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `vide_grenier`
--

-- --------------------------------------------------------

--
-- Structure de la table `mailing_list`
--

CREATE TABLE `mailing_list` (
  `ID_ML` int(11) NOT NULL,
  `MAIL_ML` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `mailing_list`
--

INSERT INTO `mailing_list` (`ID_ML`, `MAIL_ML`) VALUES
(2, 'exemple@mail.com'),
(4, 'testTrigger@test.fr'),
(5, 'ozzaxazxujz@azxoj.fr'),
(7, 'consectetuer.ipsum@laoreetipsum.net'),
(8, 'erat.vel@fermentumarcu.org'),
(9, 'imperdiet.ullamcorper.Duis@euismodest.ca'),
(10, 'mus.Proin.vel@tristiquepharetra.co.uk'),
(11, 'et.ultrices@interdumCurabitur.org'),
(12, 'interdum.Nunc@lectusjusto.co.uk'),
(13, 'cursus.et.eros@Sedcongueelit.edu'),
(14, 'vitae@luctusvulputatenisi.edu'),
(15, 'eget.venenatis@sit.org'),
(16, 'consectetuer@tortornibh.com'),
(17, 'nulla.Integer@justo.edu'),
(18, 'Nunc.pulvinar@augue.com'),
(19, 'placerat.Cras@tellusPhaselluselit.org'),
(20, 'erat.eget.tincidunt@molestiein.net'),
(21, 'diam.Sed@enimgravidasit.edu'),
(22, 'porttitor.eros@idmagnaet.org'),
(23, 'egestas@eu.com'),
(24, 'dictum@malesuadaaugueut.co.uk'),
(25, 'dictum.Phasellus.in@intempus.com'),
(26, 'tempus@turpis.edu'),
(27, 'non@sit.net'),
(28, 'non@semconsequatnec.org'),
(29, 'amet@facilisis.org'),
(30, 'risus@ipsumleo.edu'),
(31, 'in.magna.Phasellus@Nunc.org'),
(32, 'dui@semvitaealiquam.net'),
(33, 'ultrices.iaculis.odio@maurisInteger.com'),
(34, 'leo.in@lectus.co.uk'),
(35, 'dolor.sit@Suspendissealiquet.org'),
(36, 'Curabitur.vel.lectus@laciniamattisInteger.com'),
(37, 'metus@necmetusfacilisis.edu'),
(38, 'consectetuer@vitaemauris.net'),
(39, 'sem.egestas.blandit@parturient.org'),
(40, 'nec.urna@perconubianostra.com'),
(41, 'orci.Ut.sagittis@nunc.edu'),
(42, 'ut.cursus@elitNullafacilisi.co.uk'),
(43, 'faucibus.ut@mollisnon.co.uk'),
(44, 'Cum.sociis.natoque@Integer.co.uk'),
(45, 'est@risusNulla.com'),
(46, 'urna@arcu.com'),
(47, 'tincidunt@Pellentesquetincidunttempus.edu'),
(48, 'enim.consequat.purus@sem.com'),
(49, 'non@eleifendnunc.org'),
(50, 'Maecenas@consectetuereuismod.org'),
(51, 'felis.orci.adipiscing@vulputatemaurissagittis.edu'),
(52, 'mauris.a@mollis.org'),
(53, 'mi@utpharetrased.co.uk'),
(54, 'lobortis@tristiquesenectus.edu'),
(55, 'tincidunt@sit.org'),
(56, 'arcu@dapibusid.co.uk'),
(57, 'primis.in.faucibus@urnaconvalliserat.co.uk'),
(58, 'leo.Morbi.neque@metusIn.ca'),
(59, 'eu.nulla@turpis.co.uk'),
(60, 'et.ultrices@Aliquamrutrumlorem.net'),
(61, 'et@Sedneque.co.uk'),
(62, 'sem@turpis.net'),
(63, 'Ut@atlibero.com'),
(64, 'dictum@enim.com'),
(65, 'rutrum.non.hendrerit@sollicitudin.co.uk'),
(66, 'amet@sedtortorInteger.com'),
(67, 'netus.et@nonummyut.edu'),
(68, 'dapibus.quam.quis@elitfermentumrisus.co.uk'),
(69, 'vulputate.risus.a@ultrices.org'),
(70, 'eu.arcu.Morbi@orci.edu'),
(71, 'eu.enim@disparturient.co.uk'),
(72, 'eget.massa@ac.net'),
(73, 'Cras.dolor@eleifendegestasSed.ca'),
(74, 'nunc.interdum.feugiat@Quisqueimperdieterat.edu'),
(75, 'vitae.risus@Nullamenim.net'),
(76, 'Integer.aliquam.adipiscing@ornare.com'),
(77, 'ac.turpis@Donectempor.edu'),
(78, 'et.rutrum@massa.net'),
(79, 'Cras.lorem.lorem@ipsum.ca'),
(80, 'nec.leo@malesuada.co.uk'),
(81, 'neque@Nullaaliquet.com'),
(82, 'Mauris@dignissim.com'),
(83, 'sed.sem.egestas@antebibendumullamcorper.ca'),
(84, 'aliquet.molestie.tellus@urnajustofaucibus.com'),
(85, 'Nam.interdum.enim@inconsectetueripsum.net'),
(86, 'Nulla.tincidunt.neque@habitantmorbi.net'),
(87, 'Ut.tincidunt@dignissimpharetraNam.ca'),
(88, 'Duis@tinciduntorciquis.net'),
(89, 'elit.Etiam@NullainterdumCurabitur.co.uk'),
(90, 'neque.pellentesque@velit.com'),
(91, 'a.felis.ullamcorper@facilisisSuspendisse.edu'),
(92, 'nec.cursus@at.edu'),
(93, 'ultrices@maurisrhoncusid.edu'),
(94, 'Donec@anteNunc.edu'),
(95, 'eget.magna.Suspendisse@Vivamussit.com'),
(96, 'elit.Etiam@egetipsumSuspendisse.com'),
(97, 'elit.pellentesque@Duis.com'),
(98, 'mi.ac.mattis@dictum.co.uk'),
(99, 'in.magna@amet.co.uk'),
(100, 'nec@feugiatSed.co.uk'),
(101, 'Etiam.vestibulum.massa@VivamusnisiMauris.co.uk'),
(102, 'amet.orci@pede.ca'),
(103, 'sed.orci.lobortis@maurisSuspendissealiquet.com'),
(104, 'lacus.Cras@duinec.ca'),
(105, 'enim.Etiam@Duisdignissimtempor.ca'),
(106, 'non.lacinia.at@arcuSed.com');

-- --------------------------------------------------------

--
-- Structure de la table `reservation_vg`
--

CREATE TABLE `reservation_vg` (
  `ID_RESA` int(11) NOT NULL,
  `ID_VG` int(11) NOT NULL,
  `ID_UTIL` int(11) NOT NULL,
  `NOM_RESA` varchar(50) NOT NULL,
  `PRENOM_RESA` varchar(50) NOT NULL,
  `MAIL_RESA` varchar(50) NOT NULL,
  `ADRESSE_RESA` varchar(100) NOT NULL,
  `CODE_POSTAL_RESA` varchar(6) NOT NULL,
  `VILLE_RESA` varchar(50) NOT NULL,
  `PORTABLE_RESA` varchar(15) DEFAULT NULL,
  `CNI_RESA` varchar(12) NOT NULL,
  `DELIVRE_CNI_RESA` varchar(10) NOT NULL,
  `PAR_CNI_RESA` varchar(50) NOT NULL,
  `IMMATRICULATION_RESA` varchar(10) NOT NULL,
  `NBR_RESA` int(11) NOT NULL,
  `INFO_RESA` varchar(150) DEFAULT NULL,
  `STATU_RESA` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reservation_vg`
--

INSERT INTO `reservation_vg` (`ID_RESA`, `ID_VG`, `ID_UTIL`, `NOM_RESA`, `PRENOM_RESA`, `MAIL_RESA`, `ADRESSE_RESA`, `CODE_POSTAL_RESA`, `VILLE_RESA`, `PORTABLE_RESA`, `CNI_RESA`, `DELIVRE_CNI_RESA`, `PAR_CNI_RESA`, `IMMATRICULATION_RESA`, `NBR_RESA`, `INFO_RESA`, `STATU_RESA`) VALUES
(1, 2, 6, 'Bill', 'Jean', 'ozejfujz@dzeoj.fr', '5 rue jesaispas', '66666', 'Haha', '0645454545', '1111111111', '23/02/1990', 'Moi', 'AB-123-CD', 1, 'e', 2);

-- --------------------------------------------------------

--
-- Structure de la table `statuts`
--

CREATE TABLE `statuts` (
  `ID_STATUTS` int(11) NOT NULL,
  `LABEL_STATUTS` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `statuts`
--

INSERT INTO `statuts` (`ID_STATUTS`, `LABEL_STATUTS`) VALUES
(1, 'EN ATTENTE'),
(2, 'VALIDEE'),
(3, 'ANNULEE');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `ID_UTIL` int(11) NOT NULL,
  `MAIL_UTIL` varchar(50) NOT NULL,
  `MDP_UTIL` varchar(30) NOT NULL,
  `NOM_UTIL` varchar(50) DEFAULT NULL,
  `PRENOM_UTIL` varchar(50) DEFAULT NULL,
  `TEL_UTIL` varchar(15) DEFAULT NULL,
  `DESC_UTIL` varchar(280) DEFAULT NULL,
  `ADMIN_UTIL` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`ID_UTIL`, `MAIL_UTIL`, `MDP_UTIL`, `NOM_UTIL`, `PRENOM_UTIL`, `TEL_UTIL`, `DESC_UTIL`, `ADMIN_UTIL`) VALUES
	(2, 'adresse@mail.fr', '$2y$10$VAZf7zNAoSzioGkep5rQ5..GkrclZm7vWgVTLQFAjDkxQeT9VPkI6', NULL, 'Roger', '0222222222', 'dazdazdaz', NULL),
	(6, 'admin@admin.fr', '$2y$10$NUh1rCgEHk6HFA9bKdW7FukR1mxQf17tNf2R3jb7skDnBMgFY4J2i', 'Admin', 'Alvin', NULL, 'Admin du site', 1),
	(7, 'dee@dee.com', '$2y$10$aEGPy6lJzgXMXPmvAIRS7.sug.MEJgZ50nD1kgIYs9vR/BT4T9OIG', 'Ben', 'Roger', NULL, 'Une description', NULL),
	(10, 'testTrigger@test.fr', '$2y$10$Y27fhpOJF91aSsHVqH0kbOALIGcXTmPocBxHeI9C3BXpnX9NLnPMu', NULL, NULL, NULL, NULL, NULL),
	(11, 'ozzaxazxujz@azxoj.fr', '$2y$10$B9rpbSkcMRKyQKcUuYpdkOg.s.2lJo/3pjsGxS2KK8bFSrCgakZBS', NULL, NULL, NULL, NULL, NULL),
	(12, 'riku@chat.fr', '$2y$10$OTRkU/FNpyWTDdx9N1HC4.ALML686DQ7OsTnU0MEBEPySLiHCrTEW', 'Riku', NULL, '0222222222', NULL, 1),
	(13, 'consectetuer.ipsum@laoreetipsum.net', '$2y$10$odaVl5DSzSyiAFXiv0BIXex8ICwgbWZJugsLWml2DESDGUDNe/bIu', 'Wolf', 'Kato', '0274604684', 'ante. Maecenas mi felis, adipiscing fringilla, porttitor', NULL),
	(14, 'erat.vel@fermentumarcu.org', '$2y$10$Eno3xxQyF6CPeqvNcn4mEeE4arj1SKcUeqtkXTypb1Sv7HaMA90OS', 'Kirk', 'Elijah', '0357637435', 'neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec', NULL),
	(15, 'imperdiet.ullamcorper.Duis@euismodest.ca', '$2y$10$DB0esOO6cqRPehr9Aq9WLub14GFaqRUC.GT0NNJggfFWxnrloUmy2', 'Moore', 'Travis', '0593947317', 'fermentum vel, mauris. Integer sem elit, pharetra ut,', NULL),
	(16, 'mus.Proin.vel@tristiquepharetra.co.uk', '$2y$10$NP6H0tXSolF79UrFRc.psOM34I4QORtqFvYHrpOQn41HmgnI4CLHu', 'Camacho', 'Ryan', '0655423569', 'enim commodo hendrerit. Donec porttitor', NULL),
	(17, 'et.ultrices@interdumCurabitur.org', '$2y$10$HhUFgvWCrMCJNLW0rkmu4eEwl7GKeiklCFXPcRJtc6i2lL348S/5m', 'Gregory', 'Illiana', '0894925417', 'sollicitudin a, malesuada id, erat. Etiam', NULL),
	(18, 'interdum.Nunc@lectusjusto.co.uk', '$2y$10$mARjctXRqD99eeGllOUjb.qEG7Ypqb9hdJd7H3.8u/GDhsseq3gti', 'David', 'Chelsea', '0169555241', 'eget nisi dictum augue malesuada malesuada.', NULL),
	(19, 'cursus.et.eros@Sedcongueelit.edu', '$2y$10$Ermu5fC3aMjNmVhNc7R/1ehn9PTXD20WiItk.UKVkfk4TFHnLgxXK', 'Cantrell', 'Lucy', '0196586932', 'eleifend nec, malesuada ut, sem. Nulla', NULL),
	(20, 'vitae@luctusvulputatenisi.edu', '$2y$10$m1stVAYaOF6Rkv02HG92jur9MUvsN2cHlGl1VtOpVA97gwbDAU7z6', 'Orr', 'Dara', '0177939115', 'ligula. Nullam enim. Sed nulla ante,', NULL),
	(21, 'eget.venenatis@sit.org', '$2y$10$lL/HSF5w.XzyjAHyE1OqnOqlT2u1sgvJK2BdDMKNFhtjbLOfJQNf2', 'Becker', 'Lila', '0249147371', 'nascetur ridiculus mus. Proin vel nisl. Quisque fringilla', NULL),
	(22, 'consectetuer@tortornibh.com', '$2y$10$5LyXqtR.y0hLx.jzmcY0I.IN3vbk0zrU0mSWDYC42RLrsmuoBOiyO', 'Giles', 'Howard', '0704483664', 'aliquet libero. Integer in magna. Phasellus dolor elit, pellentesque a,', NULL),
	(23, 'nulla.Integer@justo.edu', '$2y$10$.H7ioBRloRi9A.6tHNu5ReIN0eg4FktuBqozaCnX6UylhnJ25Q2pq', 'Campos', 'Hammett', '0310766555', 'urna. Nullam lobortis quam a felis ullamcorper viverra. Maecenas', NULL),
	(24, 'Nunc.pulvinar@augue.com', '$2y$10$sfUnOIJ5B7k2zrRq4s7xF.5G053SujMGvLfKQnIfQRHV1zR2XxNkW', 'Cotton', 'Rafael', '0235108793', 'id nunc interdum feugiat. Sed nec metus facilisis lorem tristique', NULL),
	(25, 'placerat.Cras@tellusPhaselluselit.org', '$2y$10$aod3U7EBqE81KeH989McU.cV7raGKbWeDSIw9CkUX.d5N8gFbWIdq', 'Finch', 'Jayme', '0242033877', 'sem, consequat nec, mollis vitae, posuere at, velit.', NULL),
	(26, 'erat.eget.tincidunt@molestiein.net', '$2y$10$/5MMapZYs8CwpF5hdT3jw.6bfzbk6ghjF8Q3ftT0YEGgxwEqQSZMS', 'Wooten', 'Jonas', '0689753177', 'risus. Nunc ac sem ut dolor dapibus gravida. Aliquam', NULL),
	(27, 'diam.Sed@enimgravidasit.edu', '$2y$10$K2opLxb4MyymowXKr9UHwuavKJGAvoZhVAN.0pOiGvTOznQccQwSC', 'Sykes', 'Yasir', '0715437560', 'eu nibh vulputate mauris sagittis placerat.', NULL),
	(28, 'porttitor.eros@idmagnaet.org', '$2y$10$.Yo1xlxFQ0gwWj4c3sEkUecfhUU0aGS5h/t9cv7KtDqX9kNWrwObi', 'Mckee', 'Eden', '0971821933', 'enim. Etiam gravida molestie arcu. Sed eu nibh vulputate mauris', NULL),
	(29, 'egestas@eu.com', '$2y$10$/rlGWWCI4kTZQnKmHJPav.m6CQW2VqExOv4bFqKxvyaapctznHtH6', 'Lara', 'Zia', '0506930965', 'Cras interdum. Nunc sollicitudin commodo ipsum. Suspendisse non leo.', NULL),
	(30, 'dictum@malesuadaaugueut.co.uk', '$2y$10$CVQC5BmKnEQhLH1C6M48SeWzYmPpAgJ3k4opCjGeUc1p.stZxFrwW', 'Berry', 'Dale', '0745314567', 'egestas rhoncus. Proin nisl sem, consequat', NULL),
	(31, 'dictum.Phasellus.in@intempus.com', '$2y$10$1xbjaqycmn9n2mYq4FTQk.jR.GrybZIv05RtzOgUmL9ADocc1S91i', 'Haynes', 'Allegra', '0147073442', 'Aenean sed pede nec ante blandit viverra. Donec', NULL),
	(32, 'tempus@turpis.edu', '$2y$10$EKkKypBagQz5pNodL0j6N.8Nnq2B48byBt6m4G1NrPsHK/26vjvvq', 'Lowe', 'Seth', '0765878698', 'erat volutpat. Nulla facilisis. Suspendisse commodo tincidunt nibh.', NULL),
	(33, 'non@sit.net', '$2y$10$j7jDkqfmXEt7qhZnPQgFveKkeyIQ2IZTomgot5OmA4E3S7EydjjNi', 'Cox', 'Laith', '0475639313', 'hendrerit consectetuer, cursus et, magna. Praesent interdum ligula', NULL),
	(34, 'non@semconsequatnec.org', '$2y$10$AnwfdsfEfB0aQlSVpaTa.ulPp3CuACkYm2U7S8iREnmi1YxwTDhLm', 'Dejesus', 'Ryan', '0461938734', 'Cras convallis convallis dolor. Quisque tincidunt pede', NULL),
	(35, 'amet@facilisis.org', '$2y$10$10vvjHMNu2CjkwXvetfQWee9THJJlDKMrsppQOo76ehEdVbUxc2S.', 'Hammond', 'Carlos', '0280016180', 'fringilla. Donec feugiat metus sit amet ante. Vivamus', NULL),
	(36, 'risus@ipsumleo.edu', '$2y$10$iK1HVlM8KSpXK.8FgZwEHefOv11tLtdWNQKNZCcZjB8xKjP.i61hO', 'Robinson', 'Upton', '0383621525', 'neque. Nullam ut nisi a odio semper cursus. Integer mollis.', NULL),
	(37, 'in.magna.Phasellus@Nunc.org', '$2y$10$3jYxUj.rPBzn3If/lmjH3.pN5n7tfO85tV4DF0je.MR4mO2Y2Eyae', 'Alvarez', 'Roth', '0985909665', 'velit. Cras lorem lorem, luctus ut, pellentesque eget, dictum placerat,', NULL),
	(38, 'dui@semvitaealiquam.net', '$2y$10$gynXtu9g2rTgY6riOpOMRerBTlOQVuiUDZcERlfAWdRDEe9xSbKYe', 'Merritt', 'Kay', '0536939596', 'In condimentum. Donec at arcu. Vestibulum ante ipsum', NULL),
	(39, 'ultrices.iaculis.odio@maurisInteger.com', '$2y$10$bWjc8X5GeISwXq6T8cgubeI1rtCcXyvWHrBoT/m3lINiwUWHyvflW', 'Poole', 'Jerome', '0899457480', 'Sed id risus quis diam luctus', NULL),
	(40, 'leo.in@lectus.co.uk', '$2y$10$Z0aEtOeD8OvQc2OZ/gHILO9OQq/6u4g5MlHhJxhi5qcfo4jq7TH6q', 'Rodriquez', 'Violet', '0794532368', 'tempor lorem, eget mollis lectus pede et risus. Quisque', NULL),
	(41, 'dolor.sit@Suspendissealiquet.org', '$2y$10$fhyDhoCGGhNG1z4/A1iT9OZkuEwlt6iODl3zsvXBO7uNFDgw0Busq', 'Dixon', 'David', '0632004742', 'Morbi quis urna. Nunc quis arcu vel quam dignissim', NULL),
	(42, 'Curabitur.vel.lectus@laciniamattisInteger.com', '$2y$10$NwN81PIUpB1qZ6isolaQ7.RSiDE4Cc2SwxXnfGD6uvzrg3qIfsJSm', 'Conley', 'Nyssa', '0301370121', 'eu, accumsan sed, facilisis vitae, orci. Phasellus dapibus', NULL),
	(43, 'metus@necmetusfacilisis.edu', '$2y$10$z2m6m5hHNKDteWOP4nuAnOJOuv318ru2txjzMKJJwg33eDW5B9fMK', 'Farmer', 'Shaine', '0195002417', 'lobortis ultrices. Vivamus rhoncus. Donec est.', NULL),
	(44, 'consectetuer@vitaemauris.net', '$2y$10$Kate27/Hiq1IwS8MKYBPPOdmLIcxenKL8RskfCIMiJSkY1b49ovoC', 'Terry', 'Jacob', '0194767240', 'malesuada vel, convallis in, cursus et, eros.', NULL),
	(45, 'sem.egestas.blandit@parturient.org', '$2y$10$bM.7of6/3xc.kLbbwn4x3exDC4KLh.fT1JwEeeAXKBXQnJPhyZgve', 'Mcclure', 'Gavin', '0272776150', 'aliquet. Proin velit. Sed malesuada augue ut lacus. Nulla', NULL),
	(46, 'nec.urna@perconubianostra.com', '$2y$10$wDUb.lowD0vL6oMdH3U0J.ragsuO826ky9ftoUXjbFcw0OtQduGTm', 'Fox', 'Christine', '0323824468', 'interdum feugiat. Sed nec metus facilisis lorem', NULL),
	(47, 'orci.Ut.sagittis@nunc.edu', '$2y$10$E8EI2iOjqmW0uI4vLIIAQ.ogBEF52eMb.pg.cK0RAMLlUz4hWMwAe', 'Logan', 'Nasim', '0100711044', 'augue, eu tempor erat neque non quam. Pellentesque habitant', NULL),
	(48, 'ut.cursus@elitNullafacilisi.co.uk', '$2y$10$bR46h9dNGyLsXuadrqpXvOe9AB.6SOE2s9yQDfxzZoWHNE3G.MVhG', 'Koch', 'Graiden', '0514495123', 'aliquet diam. Sed diam lorem,', NULL),
	(49, 'faucibus.ut@mollisnon.co.uk', '$2y$10$usnChJEkrWL8SHWEoK4o6uy9KzXA4HDWtMwAxRIxUs8JEwn4vFZfG', 'Dillon', 'Marcia', '0716019232', 'Mauris nulla. Integer urna. Vivamus molestie dapibus ligula. Aliquam erat', NULL),
	(50, 'Cum.sociis.natoque@Integer.co.uk', '$2y$10$kUHFHvIYCvtf2Ik8n8eniOrFlzNFmCr/lQE64prqD4Un3lXkfrCH.', 'Greene', 'Uriah', '0786462272', 'nec, leo. Morbi neque tellus, imperdiet non,', NULL),
	(51, 'est@risusNulla.com', '$2y$10$29I1q9o8OXknsZ/y5duwWOHyk4BdGip10MvhOFgKnRKBJoBCQ7FY2', 'Berry', 'Yardley', '0986403700', 'arcu. Vivamus sit amet risus. Donec egestas. Aliquam', NULL),
	(52, 'urna@arcu.com', '$2y$10$I5HX66AfeH.5ZcB97rG0V.PM1pD3znPxTqhpmeZdRKabzI7hzrLvK', 'Bowers', 'Warren', '0328178389', 'eget magna. Suspendisse tristique neque venenatis', NULL),
	(53, 'tincidunt@Pellentesquetincidunttempus.edu', '$2y$10$IsXFMMj2uBIuyO4lWQxyf.XQv2IP6mVlKsib0Rov1EVkvXhgcxn2S', 'Clemons', 'Blossom', '0883367285', 'Aliquam erat volutpat. Nulla facilisis.', NULL),
	(54, 'enim.consequat.purus@sem.com', '$2y$10$qyaboTMfhjCD3VpYm7kSTeEhbiFxbk2vvSv4c.tS0SOfVfUQ4KT9e', 'Collins', 'Victor', '0509157161', 'Donec fringilla. Donec feugiat metus sit amet ante.', NULL),
	(55, 'non@eleifendnunc.org', '$2y$10$2PEJPuqVPn96MCIik3HhLeUPf2Q9EQA8KQgSiF5.Adus7yXhHA/wy', 'Steele', 'Nicholas', '0715339715', 'Aenean egestas hendrerit neque. In ornare', NULL),
	(56, 'Maecenas@consectetuereuismod.org', '$2y$10$3GuIz46PQ/T9gyft4ycFBeo1PyOpZSN5ivheQ0nUyj2lcTohPel0S', 'Day', 'Justina', '0831696384', 'Curabitur vel lectus. Cum sociis natoque penatibus et magnis', NULL),
	(57, 'felis.orci.adipiscing@vulputatemaurissagittis.edu', '$2y$10$pSD6Bap.BWmsbUWB8/Ra/OST4X9IvH5.Paoy3BMSqhgTYa8u5Jyxa', 'Gaines', 'Tanner', '0206087432', 'Mauris blandit enim consequat purus. Maecenas libero est,', NULL),
	(58, 'mauris.a@mollis.org', '$2y$10$FN2tIyYDfqLWvWKVTvKh2eIkLpjt2WcJxA8dRupvGs7hSsA0xW0ju', 'Browning', 'Patience', '0104348284', 'diam. Pellentesque habitant morbi tristique', NULL),
	(59, 'mi@utpharetrased.co.uk', '$2y$10$Isn7..lgh.rs8Je1fm05CO/RNjpAVxmbrHdld1QDQaTWT2K.xEoVq', 'Dunn', 'Mariam', '0718741325', 'in consequat enim diam vel arcu. Curabitur ut odio vel', NULL),
	(60, 'lobortis@tristiquesenectus.edu', '$2y$10$Q/xQo03HnyKHdCucu60G2On6K3Q8fjWIan5eY3ScZXvsK6YiRIll6', 'Clark', 'Alana', '0344893611', 'ornare. In faucibus. Morbi vehicula. Pellentesque', NULL),
	(61, 'tincidunt@sit.org', '$2y$10$8XfZ0DVTktNJYWLoknOQ1.onMw8un7JS7QdUhY02uOEur4WXBoSKm', 'Jensen', 'Jordan', '0268177284', 'nec urna et arcu imperdiet ullamcorper.', NULL),
	(62, 'arcu@dapibusid.co.uk', '$2y$10$lcD4j5dh2Gx.eNeV9TUPIeq68n9ZmmNmXKlL1z9xZYSAa2QKBXj/a', 'Brady', 'Erasmus', '0468765404', 'lorem eu metus. In lorem. Donec elementum, lorem ut', NULL),
	(63, 'primis.in.faucibus@urnaconvalliserat.co.uk', '$2y$10$N.GOEb3VbTzJgxy1kUjw5.52TJW4UbRWzdXg776KKhUc1vqTZVfua', 'Marquez', 'Tanek', '0284800677', 'at, iaculis quis, pede. Praesent eu dui.', NULL),
	(64, 'leo.Morbi.neque@metusIn.ca', '$2y$10$PRgmYJVrpE9eT9aSyxpas.49b/QYo5IaeNCBwGA82qh.EQycKRYNK', 'Francis', 'Preston', '0903866307', 'Aenean eget metus. In nec orci. Donec nibh.', NULL),
	(65, 'eu.nulla@turpis.co.uk', '$2y$10$HZi1uY9aakU7z4GTyPqs3eHqDOaYeEBi1rdjunKH8Sw2YBFhx858S', 'Harrington', 'Abdul', '0578544618', 'iaculis quis, pede. Praesent eu dui. Cum sociis', NULL),
	(66, 'et.ultrices@Aliquamrutrumlorem.net', '$2y$10$l9a61RBFM6Cx.YaU2WxREuDnvQ5Grth5FIGLO4f0JQh0ip/g3zvUm', 'Dillard', 'Kylie', '0325005735', 'diam. Pellentesque habitant morbi tristique', NULL),
	(67, 'et@Sedneque.co.uk', '$2y$10$lK7EJux1PrnCvnqE2oa5XO.nM4bFrG2T.OcSxQjXkIxbbRaUlzNse', 'Mejia', 'Hunter', '0137511008', 'aliquet odio. Etiam ligula tortor, dictum eu, placerat eget,', NULL),
	(68, 'sem@turpis.net', '$2y$10$0ydQIZcTFPu0rPGBfnxIcOc8Wk/lMYWD2G5QfdKcddGDdkM9COq1e', 'Goodman', 'Tad', '0718414386', 'mauris. Suspendisse aliquet molestie tellus. Aenean egestas hendrerit neque. In', NULL),
	(69, 'Ut@atlibero.com', '$2y$10$kFo2zsQqEe0pHodLYIVdouvZCAbjBrlkgjiLLGFyygxh7G8Sp3c6e', 'Trujillo', 'Shay', '0154935281', 'sed dui. Fusce aliquam, enim nec tempus scelerisque,', NULL),
	(70, 'dictum@enim.com', '$2y$10$vgnP49Js3.uHAV7PKRgTNeizM9tU.IBw/rrEweACGYfEFn1vidlGu', 'Hawkins', 'Xander', '0563215524', 'nunc id enim. Curabitur massa. Vestibulum accumsan neque', NULL),
	(71, 'rutrum.non.hendrerit@sollicitudin.co.uk', '$2y$10$YSLu5.w7ayC4c4f8gnL.ke9VJMtv6BzHb410HukONrKDr5tD2dhMq', 'Key', 'Forrest', '0996452249', 'erat volutpat. Nulla dignissim. Maecenas ornare egestas ligula. Nullam feugiat', NULL),
	(72, 'amet@sedtortorInteger.com', '$2y$10$59HJQC7udhE2RXwnShPiROt3SgB/vShqGluPqe8FD3aS6YASnibju', 'Johns', 'Claire', '0769818415', 'eu augue porttitor interdum. Sed', NULL),
	(73, 'netus.et@nonummyut.edu', '$2y$10$nEiVfULpAau1D7IZuiOBqOufZc9.ZqHc6IPWlUDimX8iBNl070OyW', 'Larson', 'Renee', '0620901032', 'dictum. Proin eget odio. Aliquam vulputate', NULL),
	(74, 'dapibus.quam.quis@elitfermentumrisus.co.uk', '$2y$10$Z3qef2OafGzCrE8xGctYyePjie3m5aDV2n1GtmczvNBsoMq0Vl1Ie', 'Franklin', 'Ariel', '0539787541', 'vehicula aliquet libero. Integer in magna. Phasellus', NULL),
	(75, 'vulputate.risus.a@ultrices.org', '$2y$10$x6Alsi7JYLD0PjBRZabBzeI1jgvFYrcDqEqOeWhT1wQkzLbjvqUaa', 'Rodriquez', 'Eleanor', '0473880870', 'Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue', NULL),
	(76, 'eu.arcu.Morbi@orci.edu', '$2y$10$xE.JJBsNV5BzKCqxNZ.aVOdJKb3FnQ69O3wapGdtnh9F5V4Kcwvkq', 'Mitchell', 'Kaitlin', '0626212461', 'ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere', NULL),
	(77, 'eu.enim@disparturient.co.uk', '$2y$10$J2grJ2fPI3Nu/i7LMGnrqOMDqnkXJZzKaMCs8scNYkc0tsmv0Plg2', 'Ortega', 'Plato', '0276873859', 'nec luctus felis purus ac tellus. Suspendisse sed dolor.', NULL),
	(78, 'eget.massa@ac.net', '$2y$10$51T9q4IE.aKFd2Fug7BGKODgS5r0014xtKt6It/8yPznynspngGAK', 'Tillman', 'Tiger', '0277555963', 'magna tellus faucibus leo, in lobortis', NULL),
	(79, 'Cras.dolor@eleifendegestasSed.ca', '$2y$10$SttboeSUjRrGm00ifvitf.Er7Q95vmC0a8TOM8A7ZfdvcAoK8/Qke', 'Copeland', 'Jordan', '0373223576', 'arcu vel quam dignissim pharetra. Nam', NULL),
	(80, 'nunc.interdum.feugiat@Quisqueimperdieterat.edu', '$2y$10$PujZxVIWnZ/5VaVpRVVEDuAkeQmZsDIb3kFesluFyg/4qM044ajPy', 'Curry', 'Lamar', '0474671726', 'nec, euismod in, dolor. Fusce feugiat. Lorem', NULL),
	(81, 'vitae.risus@Nullamenim.net', '$2y$10$hkzJU63LCfsQ03nPNpvHsO/2a891vYfMLuYT/Y6aSetUpxJdMJXz.', 'Petty', 'Elmo', '0648424142', 'ante dictum mi, ac mattis velit justo', NULL),
	(82, 'Integer.aliquam.adipiscing@ornare.com', '$2y$10$Jdb0Y4lWvq0UnfSVMoBrS.PR4Bz0kttg5BMa8MHvaM4CoPOIjrcF2', 'Fitzpatrick', 'Maxwell', '0606182565', 'aliquet, sem ut cursus luctus, ipsum leo elementum', NULL),
	(83, 'ac.turpis@Donectempor.edu', '$2y$10$xBiaUSAlPoGbt5rwMnz21euQBpX6vzAIurOhUebmZn0lt1fTIzlvq', 'Reyes', 'Rhoda', '0728068015', 'lobortis augue scelerisque mollis. Phasellus libero mauris, aliquam', NULL),
	(84, 'et.rutrum@massa.net', '$2y$10$9qGHX6v07NEuh1Mjs472JOW/xDzMCm8qAIcbo.8EBXGP/xC2dusKK', 'Holden', 'Tashya', '0707859535', 'nec, cursus a, enim. Suspendisse aliquet, sem ut', NULL),
	(85, 'Cras.lorem.lorem@ipsum.ca', '$2y$10$w8ObabuOKPgr4bwM4sK1rOimf6veIGtvWTMiI2r6CchKg3ZMvLSZ6', 'Gilliam', 'Grace', '0658051481', 'imperdiet, erat nonummy ultricies ornare, elit elit fermentum risus, at', NULL),
	(86, 'nec.leo@malesuada.co.uk', '$2y$10$4y.ouaGL28qYUlt/RxckveCN6k6.FE6fJdL0F9WchMobom7yWl6BC', 'Mcpherson', 'Shannon', '0249149452', 'odio. Aliquam vulputate ullamcorper magna. Sed eu', NULL),
	(87, 'neque@Nullaaliquet.com', '$2y$10$HmwNTd19nFDcn/IlcGqHnOJfuvk6qltG/U2ucQO9v8R/ctxY072gy', 'Cain', 'Lester', '0488022611', 'Duis dignissim tempor arcu. Vestibulum', NULL),
	(88, 'Mauris@dignissim.com', '$2y$10$TsxJEhUVdct4pYBgT5cZQ.H2DZUFPcoS286oWP9IAUiT4Q2HCoAte', 'Chapman', 'Cain', '0246731224', 'non, cursus non, egestas a, dui. Cras pellentesque.', NULL),
	(89, 'sed.sem.egestas@antebibendumullamcorper.ca', '$2y$10$YwCmvY2brtZdugwDviiMZ.DZ8mtekXh2Z.pNqmFXtBnANKA9fUUwu', 'Emerson', 'Kerry', '0531514682', 'eget varius ultrices, mauris ipsum porta elit, a', NULL),
	(90, 'aliquet.molestie.tellus@urnajustofaucibus.com', '$2y$10$/QOvYXlZ/e9o.aWpwb6nC.hl/tRSBLsYfUDOqs8MstZarbDYzFRRq', 'Parsons', 'Phelan', '0907178111', 'nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut', NULL),
	(91, 'Nam.interdum.enim@inconsectetueripsum.net', '$2y$10$9Q2CKmrl88DJO/Ocda0o0eTNp20RypTMujWbpMg1kE.1j2nZiHNBC', 'Hartman', 'Kitra', '0585017880', 'interdum enim non nisi. Aenean eget metus. In nec orci.', NULL),
	(92, 'Nulla.tincidunt.neque@habitantmorbi.net', '$2y$10$hSrxrUj8G3Esqr0TdSZyr.M5oxMwBKbk3amafskgVftSOTfVOPGAK', 'Hardin', 'Clio', '0673132301', 'netus et malesuada fames ac turpis', NULL),
	(93, 'Ut.tincidunt@dignissimpharetraNam.ca', '$2y$10$7B4rsnvd03totdfU40Fo3uzrTUZ48ovoYCMNTso4e6P2X8qO/00g6', 'Wilkerson', 'Myra', '0463263000', 'a tortor. Nunc commodo auctor velit. Aliquam', NULL),
	(94, 'Duis@tinciduntorciquis.net', '$2y$10$f.7yv4zQUyRCTvXuijdrFObFMIHeisXlbPZEEvS3Br7CZGxNPoupa', 'Hutchinson', 'Rafael', '0574655918', 'nulla magna, malesuada vel, convallis in, cursus et,', NULL),
	(95, 'elit.Etiam@NullainterdumCurabitur.co.uk', '$2y$10$.uf4qvOU98ptwXF7QtSU1uXodfVQILHsVblkenNgCBTC939RsBzWe', 'Logan', 'Neve', '0254282323', 'tempor diam dictum sapien. Aenean', NULL),
	(96, 'neque.pellentesque@velit.com', '$2y$10$mr.umQokNZXqGj4xIimhQuiB8cmmOO6LAh6r.x0dm8APuMIn2VCSq', 'Moon', 'Ifeoma', '0715515929', 'tellus. Phasellus elit pede, malesuada vel, venenatis vel,', NULL),
	(97, 'a.felis.ullamcorper@facilisisSuspendisse.edu', '$2y$10$DrSVfpCnxxFon7R4fG29dOwdgdfqhvGtiAusZl.BbSexRlwqcQeCy', 'Short', 'Ignacia', '0630798185', 'interdum enim non nisi. Aenean eget', NULL),
	(98, 'nec.cursus@at.edu', '$2y$10$gKNYHKoL6kRGCcmREzoYdee3KoKgFLiTDLBa8WQ7W.JZ1vj/lAC9u', 'Frank', 'Miriam', '0757085076', 'dui. Fusce aliquam, enim nec tempus scelerisque,', NULL),
	(99, 'ultrices@maurisrhoncusid.edu', '$2y$10$64MYMka3WN2FF5xZQXNZc..x088CRLYUMu/wj.P64jpgvLyOXkP3i', 'Keith', 'Jolie', '0310347261', 'Maecenas mi felis, adipiscing fringilla, porttitor', NULL),
	(100, 'Donec@anteNunc.edu', '$2y$10$V0SQQ4qtxs0GSCp0lsMSDOSqJGF1QNDY0wIJt0cOTaNfFiBih2wnK', 'Wilcox', 'Skyler', '0526776811', 'mus. Proin vel arcu eu odio tristique pharetra. Quisque ac', NULL),
	(101, 'eget.magna.Suspendisse@Vivamussit.com', '$2y$10$dDtPRM.UtNlUTgc6StOhw.YWMTK5d9t4mKfvDJAduI5Zys6K/93le', 'Horne', 'Idona', '0950942527', 'molestie arcu. Sed eu nibh vulputate mauris sagittis placerat. Cras', NULL),
	(102, 'elit.Etiam@egetipsumSuspendisse.com', '$2y$10$HlRH1KNT3KpMyvcUqYlDCutEcdg0Z50Tgkz/e4/fRDXxCIuNSnN1i', 'Odom', 'Madeson', '0718073107', 'nec, euismod in, dolor. Fusce feugiat. Lorem ipsum dolor sit', NULL),
	(103, 'elit.pellentesque@Duis.com', '$2y$10$mR9QwLy6bWw.c8qZfaG3IOUnavZ1Gxg17NtGSEM/ZsZpNpNjUP1Yq', 'Mcclain', 'Jordan', '0520249294', 'elit. Nulla facilisi. Sed neque. Sed eget lacus.', NULL),
	(104, 'mi.ac.mattis@dictum.co.uk', '$2y$10$WUjloT84eDs1UDfScC7dSeClPHeIXKdNQIZosIS4OBelxDSiNMfJy', 'Berry', 'Yvonne', '0246281381', 'vitae, aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet', NULL),
	(105, 'in.magna@amet.co.uk', '$2y$10$Bomoenvq0003iRdVIojsHustod/6yO1jsSDv1Kb4WCRcCTy7Hu7sG', 'Bass', 'Jordan', '0778346326', 'ullamcorper eu, euismod ac, fermentum vel, mauris. Integer sem elit,', NULL),
	(106, 'nec@feugiatSed.co.uk', '$2y$10$L18qMBv.NVg/yhFSDo7RIOTQOYmEmRssmO8S3Bo4OsZV7mnI3QuIy', 'Sexton', 'Rudyard', '0435641697', 'mollis vitae, posuere at, velit. Cras lorem', NULL),
	(107, 'Etiam.vestibulum.massa@VivamusnisiMauris.co.uk', '$2y$10$.L8gpfMi9QmkAqoZJFSBxeog6ZNEkLpaHeULLPDQ8UMbALf3tAQJC', 'Jimenez', 'Rudyard', '0504686357', 'odio. Etiam ligula tortor, dictum eu, placerat eget, venenatis', NULL),
	(108, 'amet.orci@pede.ca', '$2y$10$okLv.5XoCovLdqCiVMV/..irDmCDIY4cYrV4tq4Ts8EZ0G1sZK7ma', 'Price', 'Alec', '0929075161', 'ante. Maecenas mi felis, adipiscing fringilla, porttitor vulputate, posuere vulputate,', NULL),
	(109, 'sed.orci.lobortis@maurisSuspendissealiquet.com', '$2y$10$MNtG01zRPL0mntgkG1m5GOoq7P/ouzru4ye43QWdFUpgsd4HDHXpi', 'Rowland', 'Kylie', '0396375414', 'aliquet molestie tellus. Aenean egestas hendrerit', NULL),
	(110, 'lacus.Cras@duinec.ca', '$2y$10$DIKYt39efSUSGAQ.YiAhVeaxlQYeRVuf5lnZqNIJTHzmiWY.0GAQS', 'Mcfarland', 'May', '0265682613', 'Duis cursus, diam at pretium aliquet,', NULL),
	(111, 'enim.Etiam@Duisdignissimtempor.ca', '$2y$10$6668Mc0coAl9i2ZT49bSVeHUg3plMymtkV72eQWpMgnj.MU9sVgEW', 'Dotson', 'Keefe', '0258133673', 'ipsum. Curabitur consequat, lectus sit amet luctus vulputate, nisi', NULL),
	(112, 'non.lacinia.at@arcuSed.com', '$2y$10$74Y47v3sY4aDEdPHPfOgE.6JU.DQkXuuwlMcNvXTheKGi.1lzsJAi', 'Powell', 'Adena', '0504628589', 'Sed congue, elit sed consequat auctor, nunc', NULL);

--
-- Déclencheurs `utilisateur`
--
DELIMITER $$
CREATE TRIGGER `inscription_mailing_auto` AFTER INSERT ON `utilisateur` FOR EACH ROW BEGIN
    
  
		INSERT INTO mailing_list (MAIL_ML) VALUE(NEW.MAIL_UTIL);
    
    
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `videgrenier`
--

CREATE TABLE `videgrenier` (
  `ID_VG` int(11) NOT NULL,
  `LABEL_VG` varchar(50) DEFAULT NULL,
  `DATE_VG` varchar(11) DEFAULT NULL,
  `HEURE_DEBUT_VG` varchar(15) NOT NULL,
  `HEURE_FIN_VG` varchar(15) NOT NULL,
  `ADRESSE_VG` varchar(100) NOT NULL,
  `NBR_EMPLACEMENTS` int(11) DEFAULT NULL,
  `NBR_RESTANT_VG` int(11) NOT NULL,
  `PRIX_EMPLACEMENTS` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `videgrenier`
--

INSERT INTO `videgrenier` (`ID_VG`, `LABEL_VG`, `DATE_VG`, `HEURE_DEBUT_VG`, `HEURE_FIN_VG`, `ADRESSE_VG`, `NBR_EMPLACEMENTS`, `NBR_RESTANT_VG`, `PRIX_EMPLACEMENTS`) VALUES
(2, 'Vide-grenier annuel 2016', '2016-07-03', '06:00:00', '16:00:00', 'Esplanade de la Gravière, Avenue De Limburg., Sainte-foy-lès-lyon 69110', 100, 67, '20'),
(3, 'Test vide-grenier par Admin', '2018-02-10', '04:00:00', '23:00:00', 'Un exemple random, 44444 Ville', 102, 96, '300');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `mailing_list`
--
ALTER TABLE `mailing_list`
  ADD PRIMARY KEY (`ID_ML`);

--
-- Index pour la table `reservation_vg`
--
ALTER TABLE `reservation_vg`
  ADD PRIMARY KEY (`ID_RESA`);

--
-- Index pour la table `statuts`
--
ALTER TABLE `statuts`
  ADD PRIMARY KEY (`ID_STATUTS`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`ID_UTIL`);

--
-- Index pour la table `videgrenier`
--
ALTER TABLE `videgrenier`
  ADD PRIMARY KEY (`ID_VG`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `mailing_list`
--
ALTER TABLE `mailing_list`
  MODIFY `ID_ML` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT pour la table `reservation_vg`
--
ALTER TABLE `reservation_vg`
  MODIFY `ID_RESA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `statuts`
--
ALTER TABLE `statuts`
  MODIFY `ID_STATUTS` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `ID_UTIL` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT pour la table `videgrenier`
--
ALTER TABLE `videgrenier`
  MODIFY `ID_VG` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
