<?php


namespace Src;


use Exception;
use PDO;

class Database
{
    private static $connection;

    /**
     * Create and get PDO connection
     *
     * @throws Exception
     */
    public static function getConnection()
    {
        if(is_null(self::$connection) || !method_exists(self::$connection, 'getAttribute') || self::$connection->getAttribute(PDO::ATTR_DRIVER_NAME) !== 'mysql'){
            $config = include ROOT . '/config.php';
            $mysql = $config['mysql'];
            $dsn = 'mysql:dbname=' . $mysql['name'] . ';host=' . $mysql['host'];
            $pdo = new PDO($dsn, $mysql['user'], $mysql['password']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$connection = $pdo;
        }
        return self::$connection;
    }

    /**
     * Run query request
     *
     * @param $statement
     * @param null $class_name
     * @param bool $one
     * @return array|mixed
     * @throws Exception
     */
    public static function query($statement, $class_name = null, $one = false)
    {
        $req = self::getConnection()->query($statement);
        $class_name === null ? $req->setFetchMode(PDO::FETCH_ASSOC) : $req->setFetchMode(PDO::FETCH_CLASS, $class_name);
        return $one ? $req->fetch() : $req->fetchAll();
    }

    /**
     * Run prepare request
     *
     * @param string $statement
     * @param array $attribute
     * @param null $class_name
     * @param bool $one
     * @return array|bool|mixed
     * @throws Exception
     */
    public static function prepare(string $statement, array $attribute, $class_name = null, $one = false)
    {
        $req = self::getConnection()->prepare($statement);
        $res = $req->execute($attribute);
        if(
            strpos($statement, 'UPDATE') === 0 ||
            strpos($statement, 'INSERT') === 0 ||
            strpos($statement, 'DELETE') === 0
        ){
            return $res;
        }
        $class_name === null ? $req->setFetchMode(PDO::FETCH_ASSOC) : $req->setFetchMode(PDO::FETCH_CLASS, $class_name);
        return $one ? $req->fetch() : $req->fetchAll();
    }
}