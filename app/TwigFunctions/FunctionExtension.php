<?php


namespace App\TwigFunctions;


use Exception;
use Src\Model;
use Src\Router;
use Src\AuthManager;
use Src\Facades\Route;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class FunctionExtension extends AbstractExtension
{
    public function getFunctions()
    {
        $twigFunctions = [];
        $names = ['asset', 'auth', 'route', 'currentRoute', 'json', 'count', '_env'];
        foreach ($names as $name){
            $twigFunctions[] = new TwigFunction($name, [$this, $name], ['is_safe' => ['html']]);
        }
        return $twigFunctions;
    }

    public function asset(string $path): string
    {
        return BASE_ULR . "asset/" . trim($path, '/');
    }

    /**
     * @param bool|null $isLogin
     * @return bool|Model|Router|null
     * @throws Exception
     */
    public function auth(bool $isLogin = null)
    {
        $auth = new AuthManager();
        if(!is_null($isLogin)){
            return $auth->isLogin() === $isLogin;
        }
        return $auth->getAuth() ?: false;
    }

    public function route($routeName, $params = []){
        return Route::generateUrlByName($routeName, $params);
    }

    public function currentRoute($route = ''){
        return Route::currentRoute($route);
    }

    public function json($data){
        return json_encode($data);
    }

    public function count($data){
        return count($data);
    }

    public function _env(string $variable, string $default = null)
    {
        return _env($variable, $default);
    }

}