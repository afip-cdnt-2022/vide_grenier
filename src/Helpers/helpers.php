<?php


if(!function_exists('_env')){
    function _env($variable, $default = null){
        $root = str_replace('public', '', getcwd());
        (new \Src\DotEnv($root . '/.env'))->load();
        if(!getenv($variable)){
            return $default;
        }
        return getenv($variable);
    }
}

if(!function_exists('config')){
    function config(string $configName)
    {
        try {
            $root = str_replace('public', '', getcwd());
            $config = include $root.'/config.php';
            if(!is_null($config)){
                $configName = array_filter(explode('.', $configName));
                foreach ($configName as $c) {
                    if(isset($config[$c])){
                        $config = $config[$c];
                    } else {
                        return null;
                    }
                }
            }
            return $config;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}

if(!function_exists('auth')){
    function auth(){
        $auth = new \Src\AuthManager();
        return $auth->getAuth();
    }
}