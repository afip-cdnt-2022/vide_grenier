<?php

namespace Src\Facades;

use Src\Router as SrcRouter;


/**
 * @method static \Src\Route get(string $path, array $action, string $name = null)
 * @method static \Src\Route post(string $path, array $action, string $name = null)
 * @method static bool namespaceRoute(string $route = '')
 * @method static \Src\Route[] getRoutes()
 * @method static string generateUrlByName(string $routeName, array $params = [])
 * @method static bool|string currentRoute(string $route = '')
 * @method static void run(\Psr\Http\Message\ServerRequestInterface $request)
 * @method static \Src\Router redirectName(string $name)
 * @method static \Src\Router redirect(string $url)
 * @method static \Src\Router back()
 *
 * @see \Src\Router
 */
class Route{

    /**
     * @var SrcRouter|null
     */
    public static ?SrcRouter $router;

    /**
     * @param $method
     * @param $arguments
     * @return void
     */
    public static function __callStatic($method, $arguments)
    {
        if(empty(self::$router)){
            self::$router = new SrcRouter();
        }
        return call_user_func_array([self::$router, $method], $arguments);
    }
}