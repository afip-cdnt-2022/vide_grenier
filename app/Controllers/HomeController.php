<?php


namespace App\Controllers;


use Src\View;

class HomeController
{
    public function index()
    {
        return View::render('accueil');
    }

    public function info()
    {
        return View::render('infoCIL');
    }
}