<?php


namespace App\Controllers;


use App\Models\Reservation;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class ReservationController
{

    public function __construct()
    {
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }
    }

    public function show($id){
        $select_attente =  "SELECT * FROM reservation_vg JOIN videgrenier ON reservation_vg.id_vg = videgrenier.id_vg JOIN statuts ON reservation_vg.statu_resa = statuts.id_statuts WHERE STATU_RESA = 1 AND ID_RESA = :id_resa";
        $reservation = Database::prepare($select_attente, ['id_resa' => $id], Reservation::class, true);
        return View::render('admin/voir_resa', compact('reservation'));
    }


    public function update($id, array $data)
    {
        Reservation::update($id, $data);
        return View::render('admin/update_resa');
    }
}