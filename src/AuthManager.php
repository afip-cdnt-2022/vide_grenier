<?php


namespace Src;


use App\Models\Utilisateur;
use Exception;
use ReflectionException;
use Src\Facades\Route;

class AuthManager
{

    private static ?AuthManager $_instance = null;
    /**
     * @var string|null
     */
    private ?string $auth = null;

    public function __construct()
    {
        $this->auth = Session::getInstance()->get('auth');
    }

    public static function getInstance()
    {
        if(is_null(self::$_instance)){
            self::$_instance = new static();
        }
        return self::$_instance;
    }

    /**
     * Get logged user
     *
     * @param bool $id
     * @return Model|Router|null
     * @throws ReflectionException
     */
    public function getAuth(bool $id = false)
    {
        if($this->isLogin()){
            if($id){
                return $this->auth;
            }
            return !is_null($this->auth) ? (Utilisateur::find($this->auth) ?? $this->logout()) : $this->logout();
        }
        return null;
    }

    /**
     * Log user to session
     *
     * @param mixed $logId
     * @return void
     */
    public function login($logId)
    {
        if(!$this->isLogin()){
            $this->auth = $logId;
            Session::getInstance()->set('auth', $logId);
        }
    }

    /**
     * logout user from session
     *
     * @return Router
     */
    public function logout()
    {
        Session::getInstance()->delete('auth');
        $this->auth = null;
        if(!Route::currentRoute(Route::generateUrlByName('home.index'))){
            return Route::redirectName('auth.signIn');
        }
        return Route::back();
    }

    /**
     * Check if logged
     *
     * @return bool
     */
    public function isLogin()
    {
        return !is_null($this->auth);
    }
}