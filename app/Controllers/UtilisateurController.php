<?php


namespace App\Controllers;


use App\Models\Utilisateur;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class UtilisateurController
{
    public function new()
    {
        return View::render('account/inscription');
    }

    public function create(array $data)
    {
        $doublon = Database::prepare("SELECT * FROM utilisateur WHERE mail_util = :mail", ['mail' => $data['mail_util']], null, true);
        if($doublon !== false){
            return Route::back()->withError(['erreur_inscription' => '*Cette adresse e-mail est déjà utilisée']);
        }
        Utilisateur::create($data);
        return View::render('verif_inscription');
    }

    public function show()
    {
        if(!AuthManager::getInstance()->isLogin()){
            return Route::redirectName('home.index');
        }
        $prepare = 'SELECT * FROM reservation_vg JOIN videgrenier ON reservation_vg.id_vg = videgrenier.id_vg JOIN statuts ON reservation_vg.statu_resa = statuts.id_statuts WHERE id_util = :id';
        $reservations = Database::prepare($prepare, ['id' => AuthManager::getInstance()->getAuth(true)]);
        return View::render('account/mon_compte', compact('reservations'));
    }

    public function update(array $data)
    {
        $auth = AuthManager::getInstance()->getAuth();
        if(!isset($data['MAIL_UTIL']) || ($data['old_password'] !== $auth->MDP_UTIL)){
            return Route::back()->withError(['erreur_update_inscription' => '*Email ou Mot de passe incorrecte']);
        }

        if(isset($data['new_password']) && ($data['new_password'] !== $data['repeat_password'])){
            return Route::back()->withError(['erreur_update_inscription' => 'Confirmer le mot de passe']);
        }
        $data['new_password'] = password_hash($data['new_password'], PASSWORD_DEFAULT);
        $data['MDP_UTIL'] = $data['new_password'] ?? $data['old_password'];
        unset($data['new_password']);
        Utilisateur::update($auth->ID_UTIL, $data);

        return View::render('update_inscription');
    }

    public function delete(array $data)
    {
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }

        Utilisateur::delete($data['choix']);
        return View::render('admin/erase_util');
    }
}