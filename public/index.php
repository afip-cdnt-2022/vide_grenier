<?php

use GuzzleHttp\Psr7\ServerRequest;
use Src\Facades\Route;

require dirname(__DIR__) . '/vendor/autoload.php';
defined('ROOT') or define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/..');
defined('BASE_ULR') or define('BASE_ULR', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/');
include ROOT . '/routes.php';
if(php_sapi_name() !== 'cli'){
    Route::run(ServerRequest::fromGlobals());
}