<?php


namespace App\Controllers;


use App\Models\Mail;
use App\Models\Reservation;
use App\Models\Utilisateur;
use App\Models\VideGrenier;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class AdminController
{
    public function __construct()
    {
        if(!AuthManager::getInstance()->getAuth()->ADMIN_UTIL){
            return Route::redirectName('home.index');
        }
    }

    public function index()
    {
        return View::render('admin/dashboard');
    }

    public function new()
    {
        return View::render('admin/prog_vg');
    }

    public function liste()
    {
        $videgreniers = VideGrenier::all();
        return View::render('admin/liste_vg', compact('videgreniers'));
    }

    public function editVg($id){
        $videgrenier = VideGrenier::find($id);
        return View::render('admin/update_vg', compact('videgrenier'));
    }

    public function utilisateurs()
    {
        $resultatCount = Database::query('SELECT COUNT(*) as count FROM utilisateur', null, true);

        $nbrPage = (int) ($resultatCount['count'] / 25) + 1;
        $page = $_GET['page'] ?? 1;
        $nbrLigneBase = ($page - 1) * 3;
        $utilisateurs = Database::query("SELECT * FROM utilisateur WHERE ADMIN_UTIL IS NULL LIMIT $nbrLigneBase, 25", Utilisateur::class);
        return View::render('admin/utilisateurs', compact('utilisateurs', 'page','nbrPage' ));
    }

    public function attentes()
    {
        $select_attente =  "SELECT * FROM reservation_vg JOIN videgrenier ON reservation_vg.id_vg = videgrenier.id_vg JOIN statuts ON reservation_vg.statu_resa = statuts.id_statuts WHERE STATU_RESA = 1";
        $attentes = Database::query($select_attente, Reservation::class);
        return View::render('admin/attente', compact('attentes'));
    }

    public function mailing()
    {
        $resultatCount = Database::query('SELECT COUNT(*) as count FROM mailing_list', null, true);

        $nbrPage = (int) ($resultatCount['count'] / 25) + 1;
        $page = $_GET['page'] ?? 1;
        $nbrLigneBase = ($page - 1) * 3;
        $mails = Database::query("SELECT * FROM mailing_list LIMIT $nbrLigneBase, 25", Mail::class);

        return View::render('admin/mailing', compact('mails', 'page', 'nbrPage'));
    }
}