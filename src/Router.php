<?php


namespace Src;


use Exception;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ServerRequestInterface;

class Router
{
    private array $routes = [];

    public function __construct(array $routes = [])
    {
        $this->routes = $routes;
    }

    /**
     * Set GET method route
     *
     * @param string $path
     * @param array $action
     * @param string|null $name
     * @return Route
     * @throws Exception
     */
    public function get(string $path, array $action, string $name = ''): Route
    {
        if($this->checkIfAlreadyRouteExist('GET', $path)){
            throw new Exception("Route $path already exist in GET routes");
        }
        $route = new Route($path, $action, $name);
        $this->routes['GET'][] = $route;
        return $route;
    }

    /**
     * Set POST method route
     *
     * @param string $path
     * @param array $action
     * @param string|null $name
     * @return Route
     * @throws Exception
     */
    public function post(string $path, array $action, string $name = ''): Route
    {
        if($this->checkIfAlreadyRouteExist('POST', $path)){
            throw new Exception("Route $path already exist in POST routes");
        }
        $route = new Route($path, $action, $name);
        $this->routes['POST'][] = $route;
        return $route;
    }

    /**
     * Check if route already exist in request method
     *
     * @param string $requestMethod
     * @param string $path
     * @return bool
     */
    private function checkIfAlreadyRouteExist(string $requestMethod, string $path)
    {
        if(isset($this->routes[$requestMethod])){
            foreach ($this->routes[$requestMethod] as $route) {
                if(trim($path, '/') === trim($route->getPath(), '/')){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Generate url by route name
     *
     * @param string $routeName
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function generateUrlByName(string $routeName, array $params = []): string
    {
        foreach ($this->routes as $request => $routesByRequest) {
            foreach ($routesByRequest as $route) {
                if($route->name === $routeName){
                    $path = explode('/', $route->path);
                    foreach ($path as $key => $value) {
                        if(preg_match('/(.*)?{(.*)?}/', $value, $matches)){
                            if(!empty($params) && isset($params[$matches[2]])){
                                $path[$key] = $params[$matches[2]];
                            } else {
                                throw new Exception("No value for the parameter $matches[2]");
                            }
                        }
                    }
                    return implode('/', $path);
                }
            }
        }
        throw new Exception("Route '$routeName' not found", 404);
    }

    /**
     * Get the current route
     * or if route is the currently used
     *
     * @param string $route
     * @return bool|string
     */
    public function currentRoute(string $route = '')
    {
        $currentRoute = ServerRequest::fromGlobals()
            ->getUri()
            ->getPath();
        if($route){
            return $currentRoute === $route;
        }
        return $currentRoute;
    }

    /**
     * Run router application
     *
     * @param ServerRequestInterface $request
     * @return void|View
     * @throws Exception
     */
    public function run(ServerRequestInterface $request)
    {
        if(isset($this->routes[$request->getMethod()])){
            foreach ($this->routes[$request->getMethod()] as $route) {
                if($route->match($request)){
                    return $route->execute($request);
                }
            }
        }
        throw new Exception(sprintf('Route %s not found', $request->getUri()->getPath()), 404);
    }

    /**
     * Redirect with errors flash message
     *
     * @param array $errors
     * @return $this
     */
    public function withError(array $errors): self
    {
        foreach ($errors as $name => $message) {
            Session::getInstance()->setFlashMessageOnType(Session::FLASH_ERROR, $name, $message);
        }
        return $this;
    }

    /**
     * Redirect with success flash message
     *
     * @param array $success
     * @return $this
     */
    public function withSuccess(array $success): self
    {
        foreach ($success as $name => $message) {
            Session::getInstance()->setFlashMessageOnType(Session::FLASH_SUCCESS, $name, $message);
        }
        return $this;
    }

    /**
     * Redirect with flash message
     *
     * @param array $messages
     * @return $this
     */
    public function with(array $messages): self
    {
        foreach ($messages as $name => $message) {
            Session::getInstance()->setFlashMessageOnType(Session::FLASH_MESSAGE, $name, $message);
        }
        return $this;
    }

    /**
     * Redirect to url
     *
     * @param string $url
     */
    public function redirect(string $url): void
    {
        header("Location: $url");
    }

    /**
     * Redirect to route name url
     *
     * @param string $name
     * @return Router
     * @throws Exception
     */
    public function redirectName(string $name): self
    {
        $path = $this->generateUrlByName($name);
        header("Location: $path");
        return $this;
    }

    /**
     * Back redirection
     *
     * @return Router
     */
    public function back(): self
    {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        return $this;
    }
}