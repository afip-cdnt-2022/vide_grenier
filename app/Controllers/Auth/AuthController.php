<?php


namespace App\Controllers\Auth;


use App\Models\Utilisateur;
use Src\AuthManager;
use Src\Database;
use Src\Facades\Route;
use Src\View;

class AuthController
{
    public function login(array $data)
    {
        $utilisateur = Database::prepare('SELECT * FROM utilisateur WHERE mail_util LIKE :mail', ['mail' => $data['mail']], Utilisateur::class, true);
        $mdpverif = password_verify($data['passConnec'],$utilisateur->ID_UTIL);
        if($utilisateur !== false || $mdpverif !== false){
            AuthManager::getInstance()->login($utilisateur->ID_UTIL);
            return Route::redirectName('utilisateur.show');
        }
        return Route::back()->withError(['erreur_log' => '*Login ou Mot de passe incorrecte']);
    }

    public function signIn()
    {
        return View::render('account/connexion');
    }

    public function logout()
    {
        AuthManager::getInstance()->logout();
        return Route::redirectName('home.index');
    }
}